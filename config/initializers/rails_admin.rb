
RA_NAVIGATION = {
  movie:                    { weight: 1,   label: 'Filmes'},
  forum_doc_year:           { weight: 9,   label: 'Filmes'},
  language:                 { weight: 10,  label: 'Filmes'},
  country:                  { weight: 11,  label: 'Filmes'},
  curatorial_organization:  { weight: 12,  label: 'Filmes'},
  thematic_designation:     { weight: 13,  label: 'Filmes'},
  media_format:             { weight: 14,  label: 'Filmes'},
  user:                     { weight: 19,  label: 'Usuários'}
}

RailsAdmin.config do |config|

  config.main_app_name = ["Filme de Quintal", "admin"]

  ### Popular gems integration

  ## == Devise ==
  config.authenticate_with do
    warden.authenticate! scope: :user
  end
  config.current_user_method(&:current_user)

  ## == Cancan ==
  config.authorize_with :cancan
  
  ## == PaperTrail ==
  config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end
end
