class MoviesController < ApplicationController

  def index
    @movies = Movie.all
  end

  def search
  end

  def show
    @movie = Movie.find(params[:id])
  end

end
