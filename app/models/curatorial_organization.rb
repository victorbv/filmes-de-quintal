class CuratorialOrganization < ActiveRecord::Base

  has_many :movies, inverse_of: :curatorial_organization

  validates_presence_of :name

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:curatorial_organization][:label]
    weight RA_NAVIGATION[:curatorial_organization][:weight]
    navigation_icon 'glyphicon glyphicon-folder-close'
    parent Movie
    list do
      field :name
      field :acronym
      field :years
      field :created_at
      field :updated_at
    end
    edit do
      field :name
      field :acronym
      field :years
    end
  end

end
