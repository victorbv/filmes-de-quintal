class ForumDocYear < ActiveRecord::Base

  has_and_belongs_to_many :exhibited_movies, class_name: "Movie", join_table: "forum_doc_exhibition_years_movies"
  has_many :not_selected_movies, class_name: "Movie", foreign_key: "year_not_selected_id"

  validates_presence_of :year

  validates :year, 
    presence: true,
    inclusion: { in: 1900..(Date.today.year + 2) },
    format: { 
      with: /(19|20)\d{2}/i, 
      message: "Deve ser um ano de quatro dígitos"
    }


  def custom_label_method
    return nil unless self.persisted?
    self.year
  end  

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:forum_doc_year][:label]
    weight RA_NAVIGATION[:forum_doc_year][:weight]
    navigation_icon 'glyphicon glyphicon-calendar'
    parent Movie
    object_label_method :custom_label_method
    list do
      field :year
      field :created_at
      field :updated_at
    end
    edit do
      field :year
    end
  end

end
