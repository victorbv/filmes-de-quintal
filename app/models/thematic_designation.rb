class ThematicDesignation < ActiveRecord::Base

  has_many :movies, inverse_of: :thematic_designation

  validates_presence_of :name

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:thematic_designation][:label]
    weight RA_NAVIGATION[:thematic_designation][:weight]
    navigation_icon 'glyphicon glyphicon-th-list'
    parent Movie
    list do
      field :name
      field :created_at
      field :updated_at
    end
    edit do
      field :name
    end
  end

end
