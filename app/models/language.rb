class Language < ActiveRecord::Base

  has_and_belongs_to_many :movies_with_original_languages, class_name: "Movie", join_table: "movies_original_languages"
  has_and_belongs_to_many :movies_with_subtitle_languages, class_name: "Movie", join_table: "movies_subtitle_languages"

  validates_presence_of :name

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:language][:label]
    weight RA_NAVIGATION[:language][:weight]
    navigation_icon 'glyphicon glyphicon-bullhorn'
    parent Movie
    list do
      field :name
      field :created_at
      field :updated_at
    end
    edit do
      field :name
    end
  end

end
