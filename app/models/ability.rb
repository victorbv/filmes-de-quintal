class Ability
  include CanCan::Ability

  def initialize(user)

    user ||= User.new

    cannot :manage, :all
    can :access, :rails_admin
    
    if user.persisted?

      can :dashboard

      if user.is? "Estagiário"

        can [:list, :read], :all
        can :update, User, id: user.id
        cannot :history, :all

      elsif user.is? :administrador

        can :manage, :all
        cannot [:update, :destroy, :history], User, role: "super_admin"

      elsif user.is? :super_admin

        can :manage, :all

      end

      cannot :show_in_app, :all
      
    end

    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/CanCanCommunity/cancancan/wiki/Defining-Abilities
  end
end
