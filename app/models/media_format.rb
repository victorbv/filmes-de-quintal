class MediaFormat < ActiveRecord::Base

  has_many :original_display_format_movies, class_name: "Movie", foreign_key: "original_display_format_id"
  has_many :collection_copy_format_movies, class_name: "Movie", foreign_key: "collection_copy_format_id"

  validates_presence_of :name

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:media_format][:label]
    weight RA_NAVIGATION[:media_format][:weight]
    navigation_icon 'glyphicon glyphicon-cd'
    parent Movie
    list do
      field :name
      field :created_at
      field :updated_at
    end
    edit do
      field :name
    end
  end

end
