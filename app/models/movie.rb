class Movie < ActiveRecord::Base

  belongs_to :curatorial_organization, inverse_of: :movies
  belongs_to :thematic_designation, inverse_of: :movies
  belongs_to :year_not_selected, class_name: "ForumDocYear"
  belongs_to :original_display_format, class_name: "MediaFormat"
  belongs_to :collection_copy_format, class_name: "MediaFormat"
  has_and_belongs_to_many :countries
  has_and_belongs_to_many :original_languages, class_name: "Language", join_table: "movies_original_languages"
  has_and_belongs_to_many :subtitle_languages, class_name: "Language", join_table: "movies_subtitle_languages"
  has_and_belongs_to_many :forum_doc_exhibition_years, class_name: "ForumDocYear", join_table: "forum_doc_exhibition_years_movies"

  LENGTHS = ["longa-metragem", "média-metragem", "curta-metragem"]
  NAT_TYPES = %w(Nacional Internacional)
  COLORS = ["Cor", "P&B", "Cor e P&B"]
  
  validates_presence_of :topographic_location, :original_title, :title_in_portuguese, :length, :synopsis
  validates :length, inclusion: { in: LENGTHS }
  validates :national_or_international, inclusion: { in: NAT_TYPES }, allow_blank: true
  validates :production_year, 
    inclusion: { in: 1900..(Date.today.year + 2) },
    format: { 
      with: /(19|20)\d{2}/i, 
      message: "Deve ser um ano de quatro dígitos"
    },
    allow_blank: true
  validates :color, inclusion: { in: COLORS }, allow_blank: true


  # has_paper_trail

  def length_enum; LENGTHS; end
  def national_or_international_enum; NAT_TYPES; end
  def color_enum; COLORS; end
  
  rails_admin do
    navigation_label RA_NAVIGATION[:movie][:label]
    weight RA_NAVIGATION[:movie][:weight]
    navigation_icon 'glyphicon glyphicon-film'
    list do
      field :original_title
      field :title_in_portuguese
      field :length
      field :countries
      field :production_year
      field :created_at
      field :updated_at
    end
    edit do
      field :topographic_location
      field :original_title
      field :title_in_portuguese
      field :other_titles
      field :length
      field :national_or_international
      field :countries
      field :production_year
      field :color
      field :duration
      field :synopsis
      field :synopsis_in_english
      field :screenplay
      field :producer
      field :direction
      field :photography
      field :sound
      field :editing
      field :original_languages
      field :subtitle_languages
      field :movie_version
      field :production
      field :exhibited_in_forum_doc
      field :forum_doc_exhibition_years
      field :not_available_in_collection
      field :available_at
      field :curatorial_organization
      field :year_not_selected
      field :thematic_designation
      field :major_festivals
      field :identity_profile
      field :thematic_profile
      field :notes
      field :original_display_format
      field :collection_copy_format
      field :screen_format
      field :media_duration
      field :conservation_state
      field :fund
      field :collection
      field :contact
      field :source_collection_contact
    end
  end

end
