class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
   # :registerable,
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable

  ROLES = %w[super_admin Administrador] << "Estagiário"

  validates_presence_of :name, :role
  validates :role, inclusion: { in: ROLES }

  def is?(what)
    self.role == what.to_s
  end

  def allowedRoles
    if self.is? :super_admin
      return ROLES
    elsif self.is? :administrador
      return [:administrador, :editor]
    elsif self.is? :editor
      return [:editor]
    else
      return []
    end
  end

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:user][:label]
    weight RA_NAVIGATION[:user][:weight]
    navigation_icon 'icon-user'
    list do
      field :name
      field :email
      field :role
      field :created_at
      field :updated_at
    end
    edit do
      field :name
      field :email
      field :role, :enum do
        enum do
          bindings[:view]._current_user.allowedRoles
        end
      end
      field :password
      field :password_confirmation
    end
  end


end
