class Country < ActiveRecord::Base

  has_and_belongs_to_many :movies

  validates_presence_of :name

  has_paper_trail
  
  rails_admin do
    navigation_label RA_NAVIGATION[:country][:label]
    weight RA_NAVIGATION[:country][:weight]
    navigation_icon 'icon-globe'
    parent Movie
    list do
      field :name
      field :created_at
      field :updated_at
    end
    edit do
      field :name
    end
  end

end
