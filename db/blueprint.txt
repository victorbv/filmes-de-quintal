


User

  name
  role

  * rails generate devise:install
  * rails generate devise User
  * rails g migration addFieldsToUsers name role
  * rake db:migrate
  * rails g rails_admin:install
  * rails g cancan:ability

  * bundle exec rails generate paper_trail:install --with-changes


Country
  name

  * rails g model Country name


Language
  name

  * rails g model Language name

ForumDocYear
  year:integer

  * rails g model ForumDocYear year:integer


CuratorialOrganization
  name
  acronym
  years

  * rails g model CuratorialOrganization name acronym years


ThematicDesignation
  name

  * rails g model ThematicDesignation name


MediaFormat
  name

  * rails g model MediaFormat name

Movie

  topographic_location
  original_title
  title_in_portuguese
  other_titles: text
  length (enum: longa-metragem | média-metragem | curta-metragem)
  national_or_international (enum: nacional | internacional)
  (HABTM) countries
  production_year:integer
  color (enum: Cor | P&B | Cor e P&B)
  duration: integer
  synopsis: text
  synopsis_in_english: text
  screenplay
  producer
  direction
  photography
  sound
  editing
  (HABTM) original_languages (languages)
  (HABTM) subtitle_languages (languages)
  movie_version
  production
  exhibited_in_forum_doc: boolean
  (HABTM) forum_doc_exhibition_years (forum_doc_year)
  not_available_in_collection: boolean
  available_at
  curatorial_organization: references
  year_not_selected_id: integer (forum_doc_year)
  thematic_designation: references
  major_festivals: text
  identity_profile: text
  thematic_profile
  notes: text
  original_display_format_id: integer (media_format)
  collection_copy_format_id: integer (media_format)
  screen_format (enum)
  media_duration: integer
  conservation_state
  fund (enum: Fórum.Doc | Filmes de Quintal)
  collection (enum: Programadora Brasil | Vídeo nas Aldeias | D.E.R | Jean Rouch | CTAV)
  contact: text
  source_collection_contact: text


  * rails g model Movie topographic_location original_title title_in_portuguese other_titles:text length national_or_international production_year:integer color duration:integer synopsis:text synopsis_in_english:text screenplay producer direction photography sound editing version production exhibited_in_forum_doc:boolean not_available_in_collection:boolean available_at curatorial_organization:references year_not_selected_id:integer thematic_designation:references major_festivals:text identity_profile:text thematic_profile notes:text original_display_format_id:integer collection_copy_format_id:integer screen_format media_duration:integer conservation_state fund collection contact:text source_collection_contact:text
  * rails g migration CreateCountryMovieJoinTable country:references movie:references
  * rails g migration createMovieOriginalLanguageJoinTable movie:references language:references
  * rails g migration createMovieSubtitleLanguageJoinTable movie:references language:references
  * rails g migration createForumDocExhibitionYearMovieJoinTable forum_doc_year:references movie:references
  * rails g migration ChangeMovieVersionColumnName


rails g controller Movies index search show

