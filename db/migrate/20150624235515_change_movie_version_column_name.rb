class ChangeMovieVersionColumnName < ActiveRecord::Migration
  def change
    rename_column :movies, :version, :movie_version
  end
end
