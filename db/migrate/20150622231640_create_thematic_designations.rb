class CreateThematicDesignations < ActiveRecord::Migration
  def change
    create_table :thematic_designations do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
