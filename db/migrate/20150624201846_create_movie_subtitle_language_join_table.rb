class CreateMovieSubtitleLanguageJoinTable < ActiveRecord::Migration
  def change
    create_table :movies_subtitle_languages do |t|
      t.references :movie, index: true, foreign_key: true
      t.references :language, index: true, foreign_key: true
    end
  end  
end
