class CreateMovies < ActiveRecord::Migration
  def change
    create_table :movies do |t|
      t.string :topographic_location
      t.string :original_title
      t.string :title_in_portuguese
      t.text :other_titles
      t.string :length
      t.string :national_or_international
      t.integer :production_year
      t.string :color
      t.integer :duration
      t.text :synopsis
      t.text :synopsis_in_english
      t.string :screenplay
      t.string :producer
      t.string :direction
      t.string :photography
      t.string :sound
      t.string :editing
      t.string :version
      t.string :production
      t.boolean :exhibited_in_forum_doc
      t.boolean :not_available_in_collection
      t.string :available_at
      t.references :curatorial_organization, index: true, foreign_key: true
      t.integer :year_not_selected_id
      t.references :thematic_designation, index: true, foreign_key: true
      t.text :major_festivals
      t.text :identity_profile
      t.string :thematic_profile
      t.text :notes
      t.integer :original_display_format_id
      t.integer :collection_copy_format_id
      t.string :screen_format
      t.integer :media_duration
      t.string :conservation_state
      t.string :fund
      t.string :collection
      t.text :contact
      t.text :source_collection_contact

      t.timestamps null: false
    end
  end
end
