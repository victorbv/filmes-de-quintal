class CreateCuratorialOrganizations < ActiveRecord::Migration
  def change
    create_table :curatorial_organizations do |t|
      t.string :name
      t.string :acronym
      t.string :years

      t.timestamps null: false
    end
  end
end
