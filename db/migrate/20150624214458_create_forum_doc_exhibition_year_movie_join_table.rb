class CreateForumDocExhibitionYearMovieJoinTable < ActiveRecord::Migration
  def change
    create_table :forum_doc_exhibition_years_movies do |t|
      t.references :forum_doc_year, index: true, foreign_key: true
      t.references :movie, index: true, foreign_key: true
    end
  end  
end
