class CreateMediaFormats < ActiveRecord::Migration
  def change
    create_table :media_formats do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
