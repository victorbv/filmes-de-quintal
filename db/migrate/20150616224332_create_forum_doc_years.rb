class CreateForumDocYears < ActiveRecord::Migration
  def change
    create_table :forum_doc_years do |t|
      t.integer :year

      t.timestamps null: false
    end
  end
end
