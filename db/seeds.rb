# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if User.all.empty?
  puts "Creating users:"
  user = User.create({
    name: 'Victor', 
    email: 'victorbv@gmail.com', 
    role: 'super_admin', 
    password: '123456ab', 
    password_confirmation: '123456ab'
  })
  puts user.persisted? ? "ok" : "falhou"
  user.errors.each do |error|
    puts error.to_s
  end

end


if MediaFormat.all.empty?
  puts "Creating MediaFormats:"
  MediaFormat.create([
    { name: "VHS" },
    { name: "S-VHS" },
    { name: "Betacam SP 10" },
    { name: "Betacam SP 30" },
    { name: "Betacam SP 60" },
    { name: "Betacam SP 90 " },
    { name: "Betacam Digital 32" },
    { name: "Betacam Digital 40" },
    { name: "Hi-8" },
    { name: "DVCAM" },
    { name: "HDCAM " },
    { name: "MINI DV" },
    { name: "U-Matic" },
    { name: "Película 35mm" },
    { name: "Película 16mm" },
    { name: "Película 8mm" },
    { name: "Película Super-8mm" },
    { name: "HD" },
    { name: "vídeo digital" },
    { name: "HDV" },
    { name: "DV" },
    { name: "DVCAM 40" },
    { name: "DVCAM 64 " },
    { name: "DVCAM" },
    { name: "124" },
    { name: "DVCAM 184" },
    { name: "vídeo" },
    { name: "Blu-Ray" },
    { name: "Arquivo digital em servidor" },
  ])
  puts "ok"
end
