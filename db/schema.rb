# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150624235515) do

  create_table "countries", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "countries_movies", id: false, force: :cascade do |t|
    t.integer "country_id", limit: 4, null: false
    t.integer "movie_id",   limit: 4, null: false
  end

  create_table "curatorial_organizations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.string   "acronym",    limit: 255
    t.string   "years",      limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "forum_doc_exhibition_years_movies", force: :cascade do |t|
    t.integer "forum_doc_year_id", limit: 4
    t.integer "movie_id",          limit: 4
  end

  add_index "forum_doc_exhibition_years_movies", ["forum_doc_year_id"], name: "index_forum_doc_exhibition_years_movies_on_forum_doc_year_id", using: :btree
  add_index "forum_doc_exhibition_years_movies", ["movie_id"], name: "index_forum_doc_exhibition_years_movies_on_movie_id", using: :btree

  create_table "forum_doc_years", force: :cascade do |t|
    t.integer  "year",       limit: 4
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
  end

  create_table "languages", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "media_formats", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "movies", force: :cascade do |t|
    t.string   "topographic_location",        limit: 255
    t.string   "original_title",              limit: 255
    t.string   "title_in_portuguese",         limit: 255
    t.text     "other_titles",                limit: 65535
    t.string   "length",                      limit: 255
    t.string   "national_or_international",   limit: 255
    t.integer  "production_year",             limit: 4
    t.string   "color",                       limit: 255
    t.integer  "duration",                    limit: 4
    t.text     "synopsis",                    limit: 65535
    t.text     "synopsis_in_english",         limit: 65535
    t.string   "screenplay",                  limit: 255
    t.string   "producer",                    limit: 255
    t.string   "direction",                   limit: 255
    t.string   "photography",                 limit: 255
    t.string   "sound",                       limit: 255
    t.string   "editing",                     limit: 255
    t.string   "movie_version",               limit: 255
    t.string   "production",                  limit: 255
    t.boolean  "exhibited_in_forum_doc",      limit: 1
    t.boolean  "not_available_in_collection", limit: 1
    t.string   "available_at",                limit: 255
    t.integer  "curatorial_organization_id",  limit: 4
    t.integer  "year_not_selected_id",        limit: 4
    t.integer  "thematic_designation_id",     limit: 4
    t.text     "major_festivals",             limit: 65535
    t.text     "identity_profile",            limit: 65535
    t.string   "thematic_profile",            limit: 255
    t.text     "notes",                       limit: 65535
    t.integer  "original_display_format_id",  limit: 4
    t.integer  "collection_copy_format_id",   limit: 4
    t.string   "screen_format",               limit: 255
    t.integer  "media_duration",              limit: 4
    t.string   "conservation_state",          limit: 255
    t.string   "fund",                        limit: 255
    t.string   "collection",                  limit: 255
    t.text     "contact",                     limit: 65535
    t.text     "source_collection_contact",   limit: 65535
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "movies", ["curatorial_organization_id"], name: "index_movies_on_curatorial_organization_id", using: :btree
  add_index "movies", ["thematic_designation_id"], name: "index_movies_on_thematic_designation_id", using: :btree

  create_table "movies_original_languages", force: :cascade do |t|
    t.integer "movie_id",    limit: 4
    t.integer "language_id", limit: 4
  end

  add_index "movies_original_languages", ["language_id"], name: "index_movies_original_languages_on_language_id", using: :btree
  add_index "movies_original_languages", ["movie_id"], name: "index_movies_original_languages_on_movie_id", using: :btree

  create_table "movies_subtitle_languages", force: :cascade do |t|
    t.integer "movie_id",    limit: 4
    t.integer "language_id", limit: 4
  end

  add_index "movies_subtitle_languages", ["language_id"], name: "index_movies_subtitle_languages_on_language_id", using: :btree
  add_index "movies_subtitle_languages", ["movie_id"], name: "index_movies_subtitle_languages_on_movie_id", using: :btree

  create_table "thematic_designations", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "name",                   limit: 255
    t.string   "role",                   limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      limit: 255,   null: false
    t.integer  "item_id",        limit: 4,     null: false
    t.string   "event",          limit: 255,   null: false
    t.string   "whodunnit",      limit: 255
    t.text     "object",         limit: 65535
    t.datetime "created_at"
    t.text     "object_changes", limit: 65535
  end

  add_index "versions", ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree

  add_foreign_key "forum_doc_exhibition_years_movies", "forum_doc_years"
  add_foreign_key "forum_doc_exhibition_years_movies", "movies"
  add_foreign_key "movies", "curatorial_organizations"
  add_foreign_key "movies", "thematic_designations"
  add_foreign_key "movies_original_languages", "languages"
  add_foreign_key "movies_original_languages", "movies"
  add_foreign_key "movies_subtitle_languages", "languages"
  add_foreign_key "movies_subtitle_languages", "movies"
end
