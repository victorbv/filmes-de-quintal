require 'rails_helper'

# bundle exec rspec spec/models/media_format_spec.rb

RSpec.describe MediaFormat, type: :model do
  
  before(:each) do
    @media_format = create(:media_format)
  end

  it "persists the media_format with no errors" do
    expect(@media_format).to be_valid
    expect(@media_format.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @media_format.name = ""
      expect(@media_format).not_to be_valid
    end

  end

end
