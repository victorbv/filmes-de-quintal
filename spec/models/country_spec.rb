require 'rails_helper'

# bundle exec rspec spec/models/country_spec.rb

RSpec.describe Country, type: :model do
  
  before(:each) do
    @country = create(:country)
  end

  it "persists the country with no errors" do
    expect(@country).to be_valid
    expect(@country.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @country.name = ""
      expect(@country).not_to be_valid
    end

  end

end
