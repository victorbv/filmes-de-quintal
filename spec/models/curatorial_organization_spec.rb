require 'rails_helper'

# bundle exec rspec spec/models/curatorial_organization_spec.rb

RSpec.describe CuratorialOrganization, type: :model do
  
  before(:each) do
    @curatorial_organization = create(:curatorial_organization)
  end

  it "persists the curatorial_organization with no errors" do
    expect(@curatorial_organization).to be_valid
    expect(@curatorial_organization.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @curatorial_organization.name = ""
      expect(@curatorial_organization).not_to be_valid
    end

  end

end
