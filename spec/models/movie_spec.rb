require 'rails_helper'

# bundle exec rspec spec/models/movie_spec.rb

RSpec.describe Movie, type: :model do
  
  before(:each) do
    @movie = create(:movie)
  end

  it "persists the movie with no errors" do
    expect(@movie).to be_valid
    expect(@movie.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails has no topographic_location" do
      @movie.topographic_location = ""
      expect(@movie).not_to be_valid
    end

    it "fails has no original_title" do
      @movie.original_title = ""
      expect(@movie).not_to be_valid
    end

    it "fails has no title_in_portuguese" do
      @movie.title_in_portuguese = ""
      expect(@movie).not_to be_valid
    end

    it "fails has no length" do
      @movie.length = ""
      expect(@movie).not_to be_valid
    end

    it "fails when length is not on types list" do
      @movie.length = "something"
      expect(@movie).not_to be_valid
    end
    
    it "fails when national_or_international is not on allowed list" do
      @movie.national_or_international = "something"
      expect(@movie).not_to be_valid
    end
    
    it "fails when the production_year is lower than 1900" do
      @movie.production_year = 1899
      expect(@movie).not_to be_valid
    end

    it "fails when the production_year is higher than 2 years from now" do
      @movie.production_year = Date.today.year + 3
      expect(@movie).not_to be_valid
    end    

    it "fails when color is not on list" do
      @movie.color = "something"
      expect(@movie).not_to be_valid
    end
    
    it "fails has no synopsis" do
      @movie.synopsis = ""
      expect(@movie).not_to be_valid
    end

  end

  describe "associations:" do

    it "has the correct reverse association on countries" do
      country = @movie.countries.first
      expect(country.movies.first).to eq(@movie)
    end

    it "has the correct reverse association on original_languages" do
      @movie.original_languages << create(:language)
      @movie.subtitle_languages << create(:language2)
      original_language = @movie.original_languages.first
      expect(original_language.movies_with_original_languages.first).to eq(@movie)
    end

    it "has the correct reverse association on subtitle_languages" do
      @movie.subtitle_languages << create(:language)
      @movie.original_languages << create(:language2)
      subtitle_language = @movie.subtitle_languages.first
      expect(subtitle_language.movies_with_subtitle_languages.first).to eq(@movie)
    end

    it "has the correct reverse association on forum_doc_exhibition_years" do
      @movie.forum_doc_exhibition_years << create(:forum_doc_year)
      forum_doc_exhibition_year = @movie.forum_doc_exhibition_years.first
      expect(forum_doc_exhibition_year.exhibited_movies.first).to eq(@movie)
    end

    it "has the correct reverse association on curatorial_organization" do
      curatorial_organization = create(:curatorial_organization)
      @movie.curatorial_organization = curatorial_organization
      @movie.save
      expect(curatorial_organization.movies.first).to eq(@movie)
    end

    it "has the correct reverse association on year_not_selected" do
      year_not_selected = create(:forum_doc_year)
      @movie.year_not_selected = year_not_selected
      @movie.save
      expect(year_not_selected.not_selected_movies.first).to eq(@movie)
    end

    it "has the correct reverse association on thematic_designation" do
      thematic_designation = create(:thematic_designation)
      @movie.thematic_designation = thematic_designation
      @movie.save
      expect(thematic_designation.movies.first).to eq(@movie)
    end

    it "has the correct reverse association on original_display_format" do
      media_format = create(:media_format)
      @movie.original_display_format = media_format
      @movie.save
      expect(media_format.original_display_format_movies.first).to eq(@movie)
    end

    it "has the correct reverse association on collection_copy_format" do
      media_format = create(:media_format)
      @movie.collection_copy_format = media_format
      @movie.save
      expect(media_format.collection_copy_format_movies.first).to eq(@movie)
    end

  end
end
