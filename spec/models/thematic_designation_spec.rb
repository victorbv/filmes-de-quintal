require 'rails_helper'

# bundle exec rspec spec/models/thematic_designation_spec.rb

RSpec.describe ThematicDesignation, type: :model do
  
  before(:each) do
    @thematic_designation = create(:thematic_designation)
  end

  it "persists the thematic_designation with no errors" do
    expect(@thematic_designation).to be_valid
    expect(@thematic_designation.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @thematic_designation.name = ""
      expect(@thematic_designation).not_to be_valid
    end

  end

end
