require 'rails_helper'

# bundle exec rspec spec/models/language_spec.rb

RSpec.describe Language, type: :model do
  
  before(:each) do
    @language = create(:language)
  end

  it "persists the language with no errors" do
    expect(@language).to be_valid
    expect(@language.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @language.name = ""
      expect(@language).not_to be_valid
    end

  end

end
