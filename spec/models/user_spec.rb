require 'rails_helper'

# bundle exec rspec spec/models/user_spec.rb

RSpec.describe User, type: :model do
  
  before(:each) do
    @user = create(:user)
  end

  it "persists the user with no errors" do
    expect(@user).to be_valid
    expect(@user.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without name" do
      @user.name = ""
      expect(@user).not_to be_valid
    end

    it "fails when saving without email" do
      @user.email = ""
      expect(@user).not_to be_valid
    end

    it "fails when saving with invalid email" do
      @user.email = "invalid email"
      expect(@user).not_to be_valid
    end

    it "fails when saving without role" do
      @user.role = ""
      expect(@user).not_to be_valid
    end

    it "fails when saving with unknown role" do
      @user.role = "unknown role"
      expect(@user).not_to be_valid
    end

    it "fails when saving without password" do
      @user.password = ""
      expect(@user).not_to be_valid
    end

    it "fails when password has less then 6 characters" do
      @user.password = "12345"
      @user.password_confirmation = "12345"
      expect(@user).not_to be_valid
    end

    it "fails when saving without password_confirmation" do
      @user.password_confirmation = ""
      expect(@user).not_to be_valid
    end

    it "fails when saving password differs password_confirmation" do
      @user.password_confirmation = "1231234"
      expect(@user).not_to be_valid
    end

  end

end
