require 'rails_helper'

# bundle exec rspec spec/models/forum_doc_year_spec.rb

RSpec.describe ForumDocYear, type: :model do
  
  before(:each) do
    @forum_doc_year = create(:forum_doc_year)
  end

  it "persists the forum_doc_year with no errors" do
    expect(@forum_doc_year).to be_valid
    expect(@forum_doc_year.persisted?).to eq(true)
  end

  describe "validation tests:" do

    it "fails when saving without year" do
      @forum_doc_year.year = ""
      expect(@forum_doc_year).not_to be_valid
    end

    it "fails when the year is lower than 1900" do
      @forum_doc_year.year = 1899
      expect(@forum_doc_year).not_to be_valid
    end

    it "fails when the year is higher than 2 years from now" do
      @forum_doc_year.year = Date.today.year + 3
      expect(@forum_doc_year).not_to be_valid
    end

  end

end
