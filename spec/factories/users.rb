RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :user do
      name "Victor"
      role "Estagiário"
      email "mail@gmail.com"
      password "123123"
      password_confirmation "123123"
    end

  end
end