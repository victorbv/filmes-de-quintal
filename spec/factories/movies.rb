
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :movie do
      topographic_location "MyString"
      original_title "MyString"
      title_in_portuguese "MyString"
      length "longa-metragem"
      countries {[FactoryGirl.create(:country), FactoryGirl.create(:country2)]}
      synopsis "MyText"

      factory :movie2 do
        topographic_location "MyString2"
        original_title "MyString2"
        title_in_portuguese "MyString2"
        length "longa-metragem"
        synopsis "MyText2"
       end
    end
  end

end