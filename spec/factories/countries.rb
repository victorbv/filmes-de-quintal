
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :country do
      name "MyString"
      factory :country2 do
        name "Brasil"
      end
    end

  end

end