
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :language do
      name "MyString"
      factory :language2 do
        name "Português"
      end
    end

  end

end