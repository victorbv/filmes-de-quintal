
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :curatorial_organization do
      name "MyString"
      factory :curatorial_organization2 do
        name "MyString2"
      end
    end

  end

end