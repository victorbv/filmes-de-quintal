
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :thematic_designation do
      name "MyString"
      factory :thematic_designation2 do
        name "MyString2"
      end
    end

  end

end