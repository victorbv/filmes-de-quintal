
RSpec.configure do |config|

  config.include FactoryGirl::Syntax::Methods

  FactoryGirl.define do

    factory :forum_doc_year do
      year 2014
      factory :forum_doc_year2 do
        year 2015
      end
    end

  end

end