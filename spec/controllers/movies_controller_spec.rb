require 'rails_helper'

# bundle exec rspec spec/controllers/movies_controller_spec.rb

RSpec.describe MoviesController, type: :controller do

  before(:each) do
    @movie = create(:movie)
    @movie2 = create(:movie2)
  end

  describe "GET #index" do

    it "returns http success" do
      get :index
      expect(response).to have_http_status(:success)
    end

    it "returns the correct list of movies" do
      get :index
      @movies = assigns(:movies)
      expect(@movies.first).to eq(@movie)
      expect(@movies.second).to eq(@movie2)
    end

  end

  describe "POST #search" do
    it "returns http success" do
      post :search
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #show" do
    it "returns http success" do
      get :show, id: @movie.id
      expect(response).to have_http_status(:success)
    end

    it "returns the correct movie" do
      get :show, id: @movie.id
      @movie0 = assigns(:movie)
      expect(@movie0).to eq(@movie)
    end

  end

end
